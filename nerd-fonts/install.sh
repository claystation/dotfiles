#!/bin/bash

if [[ $DOTFILES_HOME ]]; then
	basedir="${DOTFILES_HOME}/nerd-fonts"
else
	basedir="${HOME}/.dotfiles/nerd-fonts"
fi

git clone https://github.com/ryanoasis/nerd-fonts.git $basedir/fonts

source $basedir/fonts/install.sh
