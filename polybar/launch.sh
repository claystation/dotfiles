#!/bin/sh

killall -q polybar

if [ -z "$(pgrep -x polybar)" ]; then
    BAR="nord"
    for m in $(polybar --list-monitors | cut -d":" -f1); do
        MONITOR=$m polybar --reload $BAR &
    done
else
    polybar-msg cmd restart
fi
