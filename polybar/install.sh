#!/bin/bash

if [[ $DOTFILES_HOME ]]; then
	basedir="${DOTFILES_HOME}/polybar"
else
	basedir="${HOME}/.dotfiles/polybar"
fi

sudo apt install polybar

mkdir -p $HOME/.config/polybar
ln -s $basedir/config $HOME/.config/polybar/config
ln -s $basedir/launch.sh $HOME/.config/polybar/launch.sh
