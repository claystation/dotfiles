#!/bin/bash

if [[ $DOTFILES_HOME ]]; then
	basedir="${DOTFILES_HOME}/wallpapers"
else
	basedir="${HOME}/.dotfiles/wallpapers"
fi

feh --bg-fill $basedir/wall-portrait.jpg $basedir/wall-landscape.jpg

