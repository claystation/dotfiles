#!/bin/bash

if [[ $DOTFILES_HOME ]]; then
	basedir="${DOTFILES_HOME}/git"
else
	basedir="$(pwd)"
fi

ln -s $basedir/.gitconfig ~/.gitconfig
ln -s $basedir/.gitignore ~/.gitignore
