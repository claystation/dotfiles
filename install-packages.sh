#!/bin/bash

echo "Updating system"

sudo apt update
sudo apt upgrade -y

echo "Installing packages"

sudo apt install -y git \
	curl \
	wget \
	feh \
	scrot

echo "Installing Composer"

./install-composer.sh
sudo chown -R $USER: ~/.composer
echo "removing apache2 if existing"
sudo apt remove -y apache2
echo "Installing Valet linux"
composer global require cpriego/valet-linux


echo "Installing Node/Npm"
curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
sudo apt-get install -y nodejs

echo "Installing Yarn"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt update && sudo apt install yarn
