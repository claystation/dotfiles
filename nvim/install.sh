#!/bin/bash

if [[ $DOTFILES_HOME ]]; then
	basedir="${DOTFILES_HOME}/nvim"
else
	basedir="${HOME}/.dotfiles/nvim"
fi

echo "Installing NeoVim"

sudo apt install neovim

mkdir -p ${HOME}/.config/nvim/

echo "
source ~/.vimrc
" > ${HOME}/.config/nvim/init.vim

ln -s ${basedir}/.vimrc ${HOME}/.vimrc

curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

nvim +PlugInstall +UpdateRemotePlugins +qall

