call plug#begin('~/.vim/plugged')
Plug 'airblade/vim-gitgutter'                                             " Git diff in gutter
Plug 'junegunn/fzf.vim'                                                   " fzf wrapper for Vim
Plug 'rbong/vim-crystalline'                                              " Status line
Plug 'ryanoasis/vim-devicons'                                             " Devicons (needs to go last, can alter other plugins)
Plug 'sheerun/vim-polyglot'                                               " A collection of language packs for Vim
Plug 'tomtom/tcomment_vim'                                                " Provides easy to use, file-type sensible comments
Plug 'tpope/vim-endwise'                                                  " Ends certain structures automatically, like if/end
Plug 'tpope/vim-surround'                                                 " Adds surroundings like parentheses, brackets, quotes, XML tags, etc. Use with cs'
Plug 'w0rp/ale'                                                           " ALE (Asynchronous Lint Engine) is a plugin for providing linting in Neovim
Plug 'mhartington/oceanic-next'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
call plug#end()

set number
set tabstop=4
set softtabstop=-1
set shiftwidth=4
set expandtab

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

if (has("termguicolors"))
 set termguicolors
endif

syntax enable
colorscheme OceanicNext

filetype plugin on

let g:airline_theme='bubblegum'
