#!/bin/bash

if [[ $DOTFILES_HOME ]]; then
	basedir="${DOTFILES_HOME}/zsh"
else
	basedir="${HOME}/.dotfiles/zsh"
fi

sudo apt install -y zsh

chsh -s $(which zsh)
sudo chsh -s $(which zsh)

sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

ln -s $basedir/.zshrc ~/.zshrc
