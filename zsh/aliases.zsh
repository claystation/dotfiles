# Tools
alias art="php artisan"
alias dpa="docker ps -a"
alias dps="docker ps"
alias pu="./vendor/bin/phpunit"
alias k=kubectl

# Directories
alias dev="cd ~/development"
alias hu="cd ~/development/haaguit"

# Programs
alias db="(cd ~/development/utils && docker-compose -f database-compose.yml up -d)"
alias db-down="(cd ~/development/utils && docker-compose -f database-compose.yml down)"

# Other
alias src="source ~/.zshrc"
