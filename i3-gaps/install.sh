#!/bin/bash

sudo add-apt-repository ppa:kgilmer/speed-ricer
sudo apt-get update

sudo apt install i3-gaps

mkdir -p $HOME/.config/i3
ln -s $DOTFILES_HOME/i3-gaps/config $HOME/.config/i3/config

