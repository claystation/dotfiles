#!/bin/bash

if [[ $DOTFILES_HOME ]]; then
	basedir="${DOTFILES_HOME}/rofi"
else
	basedir="${HOME}/.dotfiles/rofi"
fi

sudo apt install rofi

mkdir -p $HOME/.config/rofi
ln -s $basedir/config $HOME/.config/rofi/config
ln -s $basedir/mytheme.rasi $HOME/.config/rofi/mytheme.rasi
ln -s $basedir/scripts $HOME/.config/rofi/scripts

