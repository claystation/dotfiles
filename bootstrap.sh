#!/bin/bash

export DOTFILES_HOME=$(pwd)

echo "ClayFiles\n\n"

echo "Installing packages"
./install-packages.sh

mkdir $HOME/Pictures/scrots

echo "Installing programs"
./install-programs.sh

echo "Installing ZSH and Oh-my-zsh"
./zsh/install.sh

echo "Installing nerd fonts (may take a while)"
./nerd-fonts/install.sh

echo "Symlinking new keymap"
echo "Backing up old config to $DOTFILES_HOME/xkb_backup"
sudo mv /usr/share/X11/xkb/symbols/pc $DOTFILES_HOME/xkb_backup
echo "Symlinking new config."
sudo ln -s $DOTFILES_HOME/xkb_conf /usr/share/X11/xkb/symbols/pc

echo "Installing i3"
./i3-gaps/install.sh

echo "Installing Poybar"
./polybar/install.sh

echo "Setting Backgrounds"
./wallpapers/install.sh

echo "Docker stuff"
./docker/install.sh

echo "Cleaning up apt"
sudo apt clean
sudo apt auto-remove
